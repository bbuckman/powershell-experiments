﻿<#
.SYNOPSIS
    A simple PowerShell port scanner
.DESCRIPTION
    Finds everything in a given subnet with a given port open.
.PARAMETER hostname
    Accepts hostname or IP address.  Localhost is default.
.PARAMETER cidr
    Specifies a CIDR mask.  Use the number only, e.g., 8 not /8.  24 is default.
.PARAMETER port
    Specifies a tcp port.  Use the number only, e.g., 22 not :22.  80 is default.
.PARAMETER timeout
    Specifies a timeout in milliseconds.  200 is default.
.EXAMPLE
    C:\PS> portscan -host 192.168.1.13 -cidr 16 -port 443 -timeout 200
    Scans 192.168.x.x/16 and displays all IPs with port 443 open, if they respond in 200 millseconds or less.
.NOTES
    Author: Bryan Buckman
    CIDR conversion code sourced from: https://gallery.technet.microsoft.com/scriptcenter/List-the-IP-addresses-in-a-60c5bb6b
    Date:   June 6, 2018    
#>

param (
    [string]$hostname="localhost",
    [int]$cidr=24,
    [int]$port=80,
    [int]$timeout=200,
    [string]$proto="tcp"

)

function testtcp ($hostname,$port,$timeout) {
  $requestCallback = $state = $null
  $client = New-Object System.Net.Sockets.TcpClient
  $beginConnect = $client.BeginConnect($hostname,$port,$requestCallback,$state)
  Start-Sleep -milli $timeout
  if ($client.Connected) 
    {
    # The dashes in the line below are a formatting hack.  Figure out how to get autosizing to work in a loop.
    [PSCustomObject]@{'---IP Address---'=$hostname;'---Port---'=$port;'---Open---'=$true}
    
    }
  $client.Close()
   
}

function testudp($hostname,$port,$timeout) {
    "Line 48"
    $client = New-Object System.Net.Sockets.UdpClient
    $client.Client.ReceiveTimeout = $timeout
    $beginConnect = $client.Connect($hostname, $port)
    $a = new-object system.text.asciiencoding
    $SendBytes = $a.GetBytes("$("Test")")
    $client.Send($SendBytes, $SendBytes.length)
    $remoteendpoint = New-Object system.net.ipendpoint([system.net.ipaddress]::Any,0)
    $receivebytes = $client.Receive([ref]$remoteendpoint)
    [string]$returndata = $a.GetString($receivebytes)
    "Line 56"
    $returndata
    If ($returndata) {
        [PSCustomObject]@{'---IP Address---'=$hostname;'---Port---'=$port;'---Open---'=$true}   
    }

}

function testsubnet($startaddr,$endaddr,$port,$timeout,$proto) {
    
    
    $startoctets=$startaddr.Split('.')
    $endoctets=$endaddr.Split('.')

    
    for ($i=[int]$startoctets[0]; $i -le [int]$endoctets[0];$i++) {
        for ($j=[int]$startoctets[1]; $j -le [int]$endoctets[1];$j++) {
            for ($k=[int]$startoctets[2]; $k -le [int]$endoctets[2];$k++) {
                for ($l=[int]$startoctets[3]; $l -le [int]$endoctets[3];$l++) {
                    
                    $ip = "$i.$j.$k.$l"
                    if ($proto -eq "tcp") { 
                        testtcp $ip $port $timeout
                    }
                    else {
                        $proto
                        testudp $ip $port $timeout
                    }
                }
            }
        }
    }
}

function IP-toINT64 () { 
  param ($ip) 
 
  $octets = $ip.split(".") 
  return [int64]([int64]$octets[0]*16777216 +[int64]$octets[1]*65536 +[int64]$octets[2]*256 +[int64]$octets[3]) 
} 
 
function INT64-toIP() { 
  param ([int64]$int) 

  return (([math]::truncate($int/16777216)).tostring()+"."+([math]::truncate(($int%16777216)/65536)).tostring()+"."+([math]::truncate(($int%65536)/256)).tostring()+"."+([math]::truncate($int%256)).tostring() )
} 

function cidr_convert($cidrip, $cidr) {
    #Adapted most of this from from https://gallery.technet.microsoft.com/scriptcenter/List-the-IP-addresses-in-a-60c5bb6b
    
    $ipaddr = [Net.IPAddress]::Parse($cidrip)
    $maskaddr = [Net.IPAddress]::Parse((INT64-toIP -int ([convert]::ToInt64(("1"*$cidr+"0"*(32-$cidr)),2))))     
    $networkaddr = new-object net.ipaddress ($maskaddr.address -band $ipaddr.address)
    $broadcastaddr = new-object net.ipaddress (([system.net.ipaddress]::parse("255.255.255.255").address -bxor $maskaddr.address -bor $networkaddr.address))
    $startaddr = $networkaddr.IPAddressToString
    $endaddr = $broadcastaddr.IPAddressToString
    
    return $startaddr,$endaddr
}

function dns_resolve($hostname) {
    $hostname2 = @{}
    $name = Resolve-DnsName -Name $hostname -Type A -ErrorAction SilentlyContinue
    if ($name.IPAddress) {
        
        $addresslist = $name.IPAddress | Sort-Object { [version]$_ }
        return $addresslist
        
    }
    else {
    $hostname2[0] = $hostname
        return $hostname2.Values
    } 
}

#Begin main code block 
$ip = $ipaddress = $startaddr = $endaddr = $cidrip = $null
$ipaddress = dns_resolve $hostname 
$startaddresses= @{}
$endaddresses = @{}
$addresscount = 0

foreach ($address in $ipaddress) { 

    $startaddr,$endaddr = cidr_convert $address $cidr
    #Some deduplication magic starting here in cse DNS returns multiple IPs in the same subnet
    $startaddresses[$addresscount] = $startaddr
    
    $endaddresses[$addresscount] = $endaddr
    $addresscount++
    #I'm not sure why all this is necessary.
    #Sort-Object -unique ought to work, but doesn't.
    $startaddresses2 = $startaddresses.GetEnumerator() | Select-Object Value -Unique | Sort-Object Value
    $endaddresses2 = $endaddresses.GetEnumerator() | Select-Object Value -Unique | Sort-Object Value
    
}

<#Kludge because if you use an IP instead of a hostname 
for some reason the 
startaddress2 array fills but has no length property.  Dumb.#>  
if ($startaddresses2.length) {
    $addresscount = $startaddresses2.length
}
         
#Build and print a table as an object.  Useful code.       
for ($p=0;$p -lt $addresscount;$p++) {
        $table += @( @{"---Start Address---"=$startaddresses2[$p].value;    "---End Address---"=$endaddresses2[$p].value})
}

$table.ForEach({[PSCustomObject]$_}) | Format-Table -AutoSize
   
for ($p=0;$p -lt $addresscount;$p++) {
    testsubnet $startaddresses2[$p].value $endaddresses2[$p].value $port $timeout $proto
}
   
<#toDo
Figure out the PowerShell style guide and style appropriately.
Accept multiple comma-separated values per parameter.
Banner grabbing.  Make this modular and adapted per service, e.g., SMB, FTP, HTTP, whatever.
Allow UDP scanning.
#>    


