$log = new-object system.diagnostics.eventlog("application","ServerName")

$a = "<style>"
$a = $a + "BODY{background-color:White;}"
$a = $a + "TABLE{border-width: 1px;border-style: solid;border-color: black;border-collapse: collapse;}"
$a = $a + "TH{border-width: 1px;padding: 0px;border-style: solid;border-color: black;background-color:green}"
$a = $a + "TD{border-width: 1px;padding: 0px;border-style: solid;border-color: black;background-color:white}"
$a = $a + "</style>"

$mail=new-object -comobject cdo.message
$mail.From = ""
$mail.To = ""
$mail.Subject = "Email For " + $(get-date)
$mail.htmlbody=ps | Select-Object name, cpu, handles, responding, npm, pm| sort-object cpu -descending |convertto-html  -head $a -body "<H2> PowerShell E-Mail Test</H2>"
$mail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
$mail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
$mail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = 1
$mail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = ""
$mail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = ""
$mail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "smtp.gmail.com"
$mail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 465
$mail.Configuration.Fields.Update()
$mail.send()
