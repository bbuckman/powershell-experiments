$Path = 'H:\DLP\sample-data-encoded.csv'
$Excel = $Excel = New-Object -Com Excel.Application
$Workbook = $Excel.Workbooks.Open($Path)
$page = 'sample-data-encoded'
$ws = $Workbook.worksheets | where-object {$_.Name -eq $page}
$row = 2
$col = 12
$coltext = "c" + [string]$col
$rows = $ws.range($coltext).currentregion.rows.count

for ($i = $row; $i -le $rows;$i++) {
    $cell = $ws.Cells.item($i, $col)
    $value = $cell.Value2    
    $Bytes = [System.Text.Encoding]::Unicode.GetBytes($value)
    $DecodedText = [System.Text.Encoding]::Unicode.GetString([System.Convert]::FromBase64String($value))
    $cell.Value2 = $DecodedText
    
}

$workbook.SaveAs("H:\DLP\sample-data-Decoded" + $coltext + ".csv")
$workbook.Close()
$Excel.quit()






